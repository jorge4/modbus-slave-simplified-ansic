/* 
 * File:   modbus_rtu_slave.h
 * Author: jorge
 *
 * Created on February 3, 2019, 9:35 PM
 */

#ifndef MODBUS_RTU_SLAVE_H
#define MODBUS_RTU_SLAVE_H

//#include "newxc8_header.h"
//#include "Arduino.h"
#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif
    
// HAL
extern uint8_t slave_id;
bool rs485_data_available();
uint8_t rs485_read_byte();
void rs485_write_data(uint8_t * data, uint16_t len);
uint32_t millis();

// DATA LINK LAYER?
bool is_holding_reg_address_valid(uint16_t address, uint16_t len, bool is_write_access);
uint16_t read_holding_reg(uint16_t address);
void write_holding_reg(uint16_t address, uint16_t data);


// HELPERS
inline void internal_read_holding_regs(bool for_me);
inline void internal_write_holding_reg(bool for_me);
inline void internal_write_multiple_holding_regs(bool for_me);
uint16_t compute_CRC16(uint8_t *puchMsg, uint16_t usDataLen);

typedef uint8_t byte;
#define make_word(hi,lo)  ((hi << 8) | lo)

// MISC

// EXPORT
    void modbus_rtu_slave_process();
    void modbus_slave_init();


#define WRITE true
#define READ false

#ifdef __cplusplus
}
#endif

#endif /* MODBUS_RTU_SLAVE_H */

