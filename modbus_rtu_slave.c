#include "modbus_rtu_slave.h"
uint8_t modbus_in_buffer[75];
uint8_t modbus_out_buffer[75];
int modbus_buffer_cnt = 0;
uint16_t modbus_res_cnt = 0;
uint16_t req_address;
uint32_t lastrecv;
byte req_function;
byte req_slave;


#define push_byte_to_modbus_buffer(x) modbus_out_buffer[modbus_res_cnt++] = x;

void modbus_slave_init() {
    lastrecv = millis();
    modbus_res_cnt = 0;
    modbus_buffer_cnt = 0;
}

void modbus_rtu_slave_process() {
    //    bool bvalid = true;
    while (rs485_data_available()) {
        modbus_in_buffer[modbus_buffer_cnt++] = rs485_read_byte();

        if (modbus_buffer_cnt >= 4) {
            req_slave = modbus_in_buffer[0];

            //if (req_slave == slave_id) {
            req_function = modbus_in_buffer[1];
            req_address = (uint16_t) make_word(modbus_in_buffer[2], modbus_in_buffer[3]);
            
            bool for_me = (req_slave == slave_id);

            switch (req_function) {
                case 1:break; // Read Coils
                case 2:break; // Read Discrete Inputs
                case 3: // Read Holding
                case 4: // Read Input Reg
                    internal_read_holding_regs(for_me);
                    break;
                case 5: //Write Coil
                    break;
                case 6://Write single holding
                    internal_write_holding_reg(for_me);
                    break;
                case 15:
                    break;
                case 16: //Write multiple holding
                    internal_write_multiple_holding_regs(for_me);
                    break;
            }
            //}
        }
        lastrecv = millis();
    }

    if (modbus_buffer_cnt > 0 && (millis() - lastrecv > 20u)) {
        modbus_buffer_cnt = 0;
    }
}

inline void internal_read_holding_regs(bool for_me) {
    if (modbus_buffer_cnt >= 8) {
        if (!for_me) {
            modbus_buffer_cnt = 0; //reset input buffer
            return;
        }
        uint16_t len = make_word(modbus_in_buffer[4], modbus_in_buffer[5]);
        uint16_t req_crc = make_word(modbus_in_buffer[7], modbus_in_buffer[6]);
        if (len > 0 && compute_CRC16(modbus_in_buffer, 6) == req_crc && is_holding_reg_address_valid(req_address, len, READ)) {
            uint16_t res_len = len * 2;
            modbus_res_cnt = 0; // empty modbus buffer
            push_byte_to_modbus_buffer(req_slave);
            push_byte_to_modbus_buffer(req_function);
            push_byte_to_modbus_buffer(res_len);

            for (uint16_t idx = 0; idx < len; idx++) {
                uint16_t reg_value = read_holding_reg(req_address + idx);
                push_byte_to_modbus_buffer(reg_value >> 8);
                push_byte_to_modbus_buffer(reg_value);
            }

            uint16_t res_crc = compute_CRC16(modbus_out_buffer, 3 + res_len);
            push_byte_to_modbus_buffer(res_crc);
            push_byte_to_modbus_buffer(res_crc >> 8);

            rs485_write_data(modbus_out_buffer, modbus_res_cnt);
//            uint16_t write_cnt = 0;
//            while (write_cnt < modbus_res_cnt) {
//                rs485_write_byte(modbus_out_buffer[write_cnt++]);
//            }

            modbus_buffer_cnt = 0; //reset input buffer
        }
    }
}

inline void internal_write_holding_reg(bool for_me) {
    if (modbus_buffer_cnt >= 8) {
        if (!for_me) {
            modbus_buffer_cnt = 0; //reset input buffer
            return;
        }
        uint16_t data = make_word(modbus_in_buffer[4], modbus_in_buffer[5]);
        uint16_t req_crc = make_word(modbus_in_buffer[7], modbus_in_buffer[6]);
        if (compute_CRC16(modbus_in_buffer, 6) == req_crc && is_holding_reg_address_valid(req_address, 0, WRITE)) {
            modbus_res_cnt = 0; // empty modbus buffer
            push_byte_to_modbus_buffer(req_slave);
            push_byte_to_modbus_buffer(req_function);
            push_byte_to_modbus_buffer(req_address >> 8);
            push_byte_to_modbus_buffer(req_address);
            write_holding_reg(req_address, data);
            push_byte_to_modbus_buffer(data >> 8);
            push_byte_to_modbus_buffer(data);

            uint16_t res_crc = compute_CRC16(modbus_out_buffer, 6);
            push_byte_to_modbus_buffer(res_crc);
            push_byte_to_modbus_buffer(res_crc >> 8);

            rs485_write_data(modbus_out_buffer, modbus_res_cnt);
//            uint16_t write_cnt = 0;
//            while (write_cnt < modbus_res_cnt) {
//                rs485_write_byte(modbus_out_buffer[write_cnt++]);
//            }

            modbus_buffer_cnt = 0; //reset input buffer
        }
    }
}

inline void internal_write_multiple_holding_regs(bool for_me) {
    if (modbus_buffer_cnt >= 7) {
        uint8_t byte_cnt = modbus_in_buffer[6];
        if (modbus_buffer_cnt >= 9 + byte_cnt) {
            if (!for_me) {
                modbus_buffer_cnt = 0; //reset input buffer
                return;
            }
            uint16_t len = make_word(modbus_in_buffer[4], modbus_in_buffer[5]);
            uint16_t req_crc = make_word(modbus_in_buffer[9 + byte_cnt - 1], modbus_in_buffer[9 + byte_cnt - 2]);
            if (compute_CRC16(modbus_in_buffer, 7 + byte_cnt) == req_crc && is_holding_reg_address_valid(req_address, len, WRITE)) {
                uint8_t* ptr = &modbus_in_buffer[7];
                for (uint16_t i = 0; i < len; i++) {
                    uint16_t data = make_word(*ptr, *(ptr + 1));
                    write_holding_reg(req_address + i, data);
                    ptr += 2;
                }
                modbus_res_cnt = 0; // empty modbus buffer
                push_byte_to_modbus_buffer(req_slave);
                push_byte_to_modbus_buffer(req_function);
                push_byte_to_modbus_buffer(req_address >> 8);
                push_byte_to_modbus_buffer(req_address);
                push_byte_to_modbus_buffer(len >> 8);
                push_byte_to_modbus_buffer(len);

                uint16_t res_crc = compute_CRC16(modbus_out_buffer, 6);
                push_byte_to_modbus_buffer(res_crc);
                push_byte_to_modbus_buffer(res_crc >> 8);

                rs485_write_data(modbus_out_buffer, modbus_res_cnt);
//                uint16_t write_cnt = 0;
//                while (write_cnt < modbus_res_cnt) {
//                    rs485_write_byte(modbus_out_buffer[write_cnt++]);
//                }

                modbus_buffer_cnt = 0; //reset input buffer
            }
        }
    }
}